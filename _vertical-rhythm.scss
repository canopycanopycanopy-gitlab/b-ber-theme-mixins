// Returns a unitless line-height value, relative to an element's font size.
// Calculated by using the global `$line-height-base`
// @example     line-height(11.390625em) // => 1.037037037
// @function line-height($font-size) {
//   @return $line-height-base * 1em * math.div($line-height-base, $font-size);
// }

@function line-height($font-size) {
  @return ceil(math.div($font-size, $line-height-base)) *
    (math.div($line-height-base, $font-size));
}

// Returns a value for a single line-height unit in `em`s, relative to the
// element's font-size. Useful for consistently spacing top/bottom
// margin/padding
// @example     adjust-line-height-value(0) // => 1.3125em
// @example     adjust-line-height-value(6) // => 0.1152263374em
@function adjust-line-height-value($value) {
  @return math.div($line-height-base, strip-unit(ms($value))) + 0em;
}

// Returns relative em value representing the base font size that is
// adjusted for the elements font size. Examples below assume the
// $font-size-base is 1.6em
// @example     adjust-font-size-value(0)   => 1em             // 16px
// @example     adjust-font-size-value(-1)  => 1.066666667em   // 16px
// @example     adjust-font-size-value(-2)  => 1.1377777785em  // 16px
@function adjust-font-size-value($value) {
  @return math.div(strip-unit($font-size-base), strip-unit(ms($value))) + 0em;
}

// Compose typographic styles
@mixin type-settings($value) {
  font-size: ms($value);
  line-height: line-height(ms($value));

  @if $vertical-space == true {
    margin-bottom: adjust-line-height-value($value);
  }
}

// Abstract wrappers for element spacing
// @param $n        int     How many spaces?
// @param $value    int     The modular scale index of the element
@function n-lines($n, $value) {
  @return adjust-line-height-value($value) * $n + 0em;
}

@function n-ems($n, $value) {
  @return adjust-font-size-value($value) * $n + 0em;
}

// Convenience wrappers for element spacing.
// @param $value    int     The modular scale index of the element
// @example         margin-top: one-line(0); // => margin-top: 1.3125em;
@function one-line($value) {
  @return n-lines(1, $value);
}
@function two-lines($value) {
  @return n-lines(2, $value);
}
@function three-lines($value) {
  @return n-lines(3, $value);
}
@function four-lines($value) {
  @return n-lines(4, $value);
}
@function five-lines($value) {
  @return n-lines(5, $value);
}

// @param   $value  int     The modular scale index of the element
// @example         margin-top: one-em(0); // => margin-top: 1em;
@function one-em($value) {
  @return n-ems(1, $value);
}

@function two-ems($value) {
  @return n-ems(2, $value);
}

@function three-ems($value) {
  @return n-ems(3, $value);
}

@function four-ems($value) {
  @return n-ems(4, $value);
}

@function five-ems($value) {
  @return n-ems(5, $value);
}

// Add padding and margins programmatically based on font-size
// @param $value                                string                  Elements type-settings name
// @param $top[, $left[, $bottom[, $right]]]    int, int, int, int      Number of line-height/font-size units
@function spacing($value, $args) {
  $result: ();

  @if length($args) > 4 {
    @error "`*-margin' and `*-padding' mixins expect maximum four arguments";
  } @else if length($args) == 1 {
    $y: adjust-line-height-value($value) * nth($args, 1) + 0em;
    $x: adjust-font-size-value($value) * nth($args, 1) + 0em;

    $result: ($y $x $y $x);
  } @else if length($args) == 2 {
    $y: adjust-line-height-value($value) * nth($args, 1) + 0em;
    $x: adjust-font-size-value($value) * nth($args, 2) + 0em;

    $result: ($y $x $y $x);
  } @else if length($args) == 3 {
    $y1: adjust-line-height-value($value) * nth($args, 1) + 0em;
    $x: adjust-font-size-value($value) * nth($args, 2) + 0em;
    $y2: adjust-line-height-value($value) * nth($args, 3) + 0em;

    $result: ($y1 $x $y2 0);
  } @else {
    $y1: adjust-line-height-value($value) * nth($args, 1) + 0em;
    $x1: adjust-font-size-value($value) * nth($args, 2) + 0em;
    $y2: adjust-line-height-value($value) * nth($args, 3) + 0em;
    $x2: adjust-font-size-value($value) * nth($args, 4) + 0em;

    $result: ($y1 $x1 $y2 $x2);
  }

  @return $result;
}

// Generic type settings
@mixin text-large {
  @include type-settings(11);
}

@mixin text-medium {
  @include type-settings(3);
}

@mixin text-default {
  @include type-settings(0);
}

@mixin text-small {
  @include type-settings(-7);
}

@mixin text-xsmall {
  @include type-settings(-8);
}

// Mapping for margin/padding mixins
$type-settings-map: (
  "text-large": 6,
  "text-medium": 3,
  "text-default": 0,
  "text-small": -7,
  "text-xsmall": -8,
);

// Legacy
// @deprecated
// Add padding and margins programmatically based on font-size
// @param $top      int     Number of line-height units at type-settings(0) to add as padding-top
// @param $left     int     Number of `em`s at type-settings(0) to add as padding-left
// @param $bottom   int     Number of line-height units at type-settings(0) to add as padding-bottom
// @param $right    int     Number of `em`s at type-settings(0) to add as padding-right
// @param $value    int     Elements type-settings index
// @example         padding: @include padding(1, 1, 1, 1, 0) // => padding: 1em 1.3125em 1em 1.3125em;
@mixin deprecated-legacy-padding($top, $right, $bottom, $left, $value) {
  padding: adjust-line-height-value($value) * $top + 0em
    adjust-font-size-value($value) * $right + 0em
    adjust-line-height-value($value) * $bottom + 0em
    adjust-font-size-value($value) * $left + 0em;
}

// Legacy
// @deprecated
@mixin deprecated-legacy-margin($top, $right, $bottom, $left, $value) {
  margin: adjust-line-height-value($value) * $top + 0em
    adjust-font-size-value($value) * $right + 0em
    adjust-line-height-value($value) * $bottom + 0em
    adjust-font-size-value($value) * $left + 0em;
}

// @example     @include margin('text-small', 0, 1);      // => margin: 0em 1em 0em 1em;
// @example     @include margin('text-small', 1);         // => margin: 1em 1em 1em 1em;
// @example     @include margin('text-small', 0, 1, 1);   // => margin: 0em 1em 1em 0em;
@mixin margin($name, $args...) {
  @if type-of($name) == "number" {
    @warn "Deprecated use of `margin` mixin. Use `margin(<name>, [int [, int [, int, [, int]]]])`";
    @include deprecated-legacy-margin($name, $args...);
  } @else {
    $numeric-value: map-get($type-settings-map, $name);
    margin: spacing($numeric-value, $args);
  }
}

@mixin padding($name, $args...) {
  @if type-of($name) == "number" {
    @warn "Deprecated use of `padding` mixin. Use `padding(<name>, [int [, int [, int, [, int]]]])`";
    @include deprecated-legacy-padding($name, $args...);
  } @else {
    $numeric-value: map-get($type-settings-map, $name);
    padding: spacing($numeric-value, $args);
  }
}

@mixin margin-top($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  margin-top: nth($spacing, 1);
}

@mixin margin-right($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  margin-right: nth($spacing, 2);
}

@mixin margin-bottom($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  margin-bottom: nth($spacing, 3);
}

@mixin margin-left($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  margin-left: nth($spacing, 4);
}

@mixin padding-top($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  padding-top: nth($spacing, 1);
}

@mixin padding-right($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  padding-right: nth($spacing, 2);
}

@mixin padding-bottom($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  padding-bottom: nth($spacing, 3);
}

@mixin padding-left($name, $arg) {
  $numeric-value: map-get($type-settings-map, $name);
  $spacing: spacing($numeric-value, $arg, $arg);
  padding-left: nth($spacing, 4);
}
